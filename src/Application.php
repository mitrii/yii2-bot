<?php

namespace mitrii\bot;

/**
 * Application is the base class for all application classes.
 *
 * @property \mitrii\bot\Bot $botManager The asset manager application component.
 *
 */
class Application extends \yii\base\Application
{

    /**
     * @var string the default route of this application. Defaults to 'help',
     * meaning the `help` command.
     */
    public $defaultRoute = 'chat/help';

    /**
     * @var Controller the currently active controller instance
     */
    public $controller;

    /**
     * Handles the specified request.
     * @param Request $request the request to be handled
     * @return Response the resulting response
     */
    public function handleRequest($request)
    {
        list ($route, $params) = $request->resolve();
        $this->requestedRoute = $route;
        
        $result = $this->runAction($route, $params);
        
        if ($result instanceof Response) {
            return $result;
        } else {
            $response = $this->getResponse();
            $response->data = $result;
            return $response;
        }
    }

    /**
     * Returns the error handler component.
     * @return ErrorHandler the error handler application component.
     */
    public function getErrorHandler()
    {
        return $this->get('errorHandler');
    }

    /**
     * Returns the request component.
     * @return Request the request component.
     */
    public function getRequest()
    {
        return $this->get('request');
    }

    /**
     * Returns the response component.
     * @return Response the response component.
     */
    public function getResponse()
    {
        return $this->get('response');
    }

    /**
     * Returns the response component.
     * @return CommandManager the command manage component.
     */
    public function getSignalManager()
    {
        return $this->get('signalManager');
    }

    /**
     * @inheritdoc
     */
    public function coreComponents()
    {
        return array_merge(parent::coreComponents(), [
            'botManager' => ['class' => 'mitrii\bot\Bot'],
            'signalManager' => ['class' => 'mitrii\bot\SignalManager'],
            'request' => ['class' => 'mitrii\bot\Request'],
            'response' => ['class' => 'mitrii\bot\Response'],
            'errorHandler' => ['class' => 'mitrii\bot\ErrorHandler'],
            'view' => ['class' => 'mitrii\bot\View'],
            'user' => ['class' => 'mitrii\bot\User'],
        ]);
    }

}
