<?php

namespace mitrii\bot\helpers;

/**
 * Poject: ready-bot-v2
 * User: mitrii
 * Date: 1.27.2017
 * Time: 13:08
 * Original File Name: Bot.php
 */
class Bot
{

    public static function encodeData($value)
    {
        return base64_encode(serialize($value));
    }

    public static function decodeData($value)
    {
        return unserialize(base64_decode($value));
    }

}