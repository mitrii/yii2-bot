<?php
/**
 * Poject: ready-bot-v2
 * User: mitrii
 * Date: 5.30.2017
 * Time: 19:18
 * Original File Name: UpdateJob.php
 */

namespace mitrii\bot\jobs;

use React\EventLoop\Factory;
use yii\helpers\Console;
use yii\base\Object;

class UpdateJob extends Object implements \yii\queue\Job
{
    public $update;

    public $route = false;

    public function execute($queue)
    {
        $app_path = \Yii::$app->botManager->botApp;
        $command = \Yii::getAlias('@vendor/mitrii/yii2-bot/bin')."/bot" .
            ' ' . escapeshellarg($app_path) .
            ' ' . escapeshellarg(base64_encode(serialize($this->update))) .
            ' ' . (($this->route !== false) ? escapeshellarg(base64_encode(serialize($this->route))) : '');

        Console::output($command);

        $process = new \React\ChildProcess\Process($command);



        $update_id = $this->update->update_id;

        $process->on('exit', function($exitCode, $termSignal) use ($process, $update_id) {
            Console::output($update_id . ' | ' . $process->getPid() . ': exti with code ' . $exitCode);
        });

        $loop = Factory::create();
        $loop->addTimer(0.001, function($timer) use ($process, $update_id) {
            $process->start($timer->getLoop());

            $pid = $process->getPid();
            Console::output($update_id . ' | '. $pid . ': start ' . $process->getCommand());

            $process->stdout->on('data', function($output) use ($pid) {
                Console::output($update_id . ' | '. $pid . ': out: ' . $output);
            });

            $process->stderr->on('data', function($output) use ($pid) {
                Console::error($update_id . ' | '. $pid . ': err: ' . $output);
            });
        });

        $loop->run();
    }
}