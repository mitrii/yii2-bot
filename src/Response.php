<?php

namespace mitrii\bot;

use unreal4u\TelegramAPI\Telegram\Methods\SendMessage;
use unreal4u\TelegramAPI\Telegram\Types\Custom\InlineKeyboardButtonArray;
use unreal4u\TelegramAPI\Telegram\Types\Custom\KeyboardButtonArray;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Button;
use unreal4u\TelegramAPI\Telegram\Types\Message;
use unreal4u\TelegramAPI\Telegram\Types\ReplyKeyboardMarkup;
use unreal4u\TelegramAPI\Telegram\Types\KeyboardButton;
use Yii;


class Response extends \yii\base\Response
{
    /**
     * @var $message SendMessage
     */
    private $_message;


    public function init()
    {
        parent::init();

        $this->message = new SendMessage();
        $this->message->chat_id = Yii::$app->getRequest()->getChatId();
    }

    public function getMessage()
    {
        return $this->_message;
    }

    public function setMessage($value)
    {
        $this->_message = $value;
    }

    public function setIsReply($value)
    {
        if ($value) {
            $this->message->reply_to_message_id = Yii::$app->request->message->message_id;
        }
        else {
            $this->message->reply_to_message_id = null;
        }
    }

    public function getIsReply()
    {
        return (!empty($this->message->reply_to_message_id));
    }

    public function setData($value)
    {
        $this->message->text = (string) $value;
    }

    /**
     * Sends the response to the client.
     */
    public function send()
    {
        if (!empty($this->message->text)) {

            $this->message->reply_markup = Yii::$app->view->markup;
            Yii::$app->botManager->api->performApiRequest($this->message);
        }
    }
}
