<?php
/**
 * Poject: ready-bot-v2
 * User: mitrii
 * Date: 12.30.2016
 * Time: 17:47
 * Original File Name: CommandRuleInterface.php
 */

namespace mitrii\bot;

/**
 * CommandRuleInterface is the interface that should be implemented by command rule classes.
 */

interface SignalRuleInterface
{
    /**
     * Parses the given request and returns the corresponding route and parameters.
     * @param CommandManager $manager the URL manager
     * @param \mitrii\bot\Request $request the request component
     * @return array|boolean the parsing result. The route and the parameters are returned as an array.
     * If false, it means this rule cannot be used to parse this path info.
     */
    public function parseRequest($manager, $request);
}