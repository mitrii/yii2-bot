<?php
namespace mitrii\bot;

use unreal4u\TelegramAPI\Telegram\Methods\GetMe;
use unreal4u\TelegramAPI\Telegram\Methods\GetUpdates;
use unreal4u\TelegramAPI\Telegram\Types\User;
use unreal4u\TelegramAPI\TgLog;
use Yii;
use \yii\helpers\StringHelper;

use \mitrii\bot\events\UpdateEvent;
use \mitrii\bot\events\MessageEvent;

/**
* @var $api unreal4u\TelegramAPI\TgLog
*/
class BotManager extends \yii\base\Component implements BotManagerInterface
{

    /**
     * @var string Path to bot app
     */
    public $botApp = '';

    /**
     * Bot token
     *
     * @var string
     */
    public $apiToken;

    public $botanApiToken;

    /**
     * @var User
     */
    private $me;

    /**
     * @var $db key-value db name
     */
    public $db = 'redis';

    /**
     * @var TgLog
     */
    private $_bot_api;

    public function getApi()
    {
        return $this->_bot_api;
    }

    public function init()
    {
        $this->_bot_api = new TgLog($this->apiToken);
        $getMe = new GetMe();
        $this->me = $this->api->performApiRequest($getMe);
    }
    
    public function getNickname()
    {
        return $this->me->username;
    }
    
    public function getBot_id()
    {
        return $this->me->id;
    }

    public function getUpdateOffset()
    {
        $offset = Yii::$app->get($this->db)->get('telegram_offset');
        return (int) $offset;
    }

    public function setUpdateOffset($value)
    {
        Yii::$app->get($this->db)->set('telegram_offset', (int) $value);
    }

    public function getUpdates()
    {
        $getUpdates = new GetUpdates();
        $getUpdates->offset = $this->updateOffset;

        $updates = $this->api->performApiRequest($getUpdates)->data;

        $offset = 0;
        if (!empty($updates)) {
            $lastUpdate = array_values(array_slice($updates, -1))[0];
            $offset = $lastUpdate->update_id+1;
        }
        $this->updateOffset = $offset;
        return $updates;
    }

    public function sendReply($message)
    {
        $sendMessage = new SendMessage();
        $sendMessage->chat_id = Yii::$app->getRequest()->getChatId();
        $sendMessage->text = $message;

        $message = $this->getApi()->performApiRequest($sendMessage);
    }

    /**
     * @deprecated
     */
    public function receiveUpdates()
    {
        /**
         * @var $updates Update[]
         */
        $updates = $this->api->getUpdates();

        foreach ($updates as $update) {
            $event = new UpdateEvent();
            $event->update = $update;
            $this->trigger(self::EVENT_RECEIVE_UPDATE, $event);

            $message = $update->getMessage();

            $events_map = [
                self::EVENT_MSG_TEXT => 'getText',
                self::EVENT_MSG_AUDIO => 'getAudio',
                self::EVENT_MSG_DOCUMENT => 'getDocument',
                self::EVENT_MSG_PHOTO => 'getPhoto',
                self::EVENT_MSG_STICKER => 'getSticker',
                self::EVENT_MSG_VIDEO => 'getVideo',
                self::EVENT_MSG_VOICE => 'getVoice',
                self::EVENT_MSG_CAPTION => 'getCaption',
                self::EVENT_MSG_CONTACT => 'getContact',
                self::EVENT_MSG_LOCATION => 'getLocation',
            ];

            foreach ($events_map as $event_id => $method_name)
            {
                if (!empty(call_user_func([$message, $method_name])))
                {
                    if (StringHelper::startsWith($message->getText(), '/')) {
                        $command = strtok($message->getText(), " \n\t");
                        
                        $args = [];                      
                        do {
                            $arg = strtok(" \n\t");
                            if ($arg !== false) $args[] = $arg;
                        } while ($arg !== false);
                        
                        $msg_event = new MessageEvent();
                        $msg_event->message = $message;
                        $msg_event->command = $command;
                        $msg_event->args = $args;
                        $this->trigger(self::EVENT_MSG_TEXT_COMMAND, $msg_event);
                        
                    }
                    else {
                        $msg_event = new MessageEvent();
                        $msg_event->message = $message;
                        $this->trigger($event_id, $msg_event);
                    }
                    

                }
            }

        }
    }
}