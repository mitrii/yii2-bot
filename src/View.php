<?php
/**
 * Poject: xbot
 * User: mitrii
 * Date: 11.08.2017
 * Time: 10:12
 * Original File Name: View.php
 */

namespace mitrii\bot;

use unreal4u\TelegramAPI\Telegram\Types\Custom\KeyboardButtonArray;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Markup;
use unreal4u\TelegramAPI\Telegram\Types\Inline\Keyboard\Button;
use unreal4u\TelegramAPI\Telegram\Types\ReplyKeyboardMarkup;
use unreal4u\TelegramAPI\Telegram\Types\Custom\InlineKeyboardButtonArray;
use yii\helpers\Json;

/**
 * @property $keyboard unreal4u\TelegramAPI\Telegram\Types\ReplyKeyboardMarkup;
 * @property $inlineKeyboard unreal4u\TelegramAPI\Telegram\Types\Custom\InlineKeyboardButtonArray;
 * @property $markup unreal4u\TelegramAPI\Telegram\Types\ReplyKeyboardMarkup;
 */
class View extends \yii\base\View
{

    /**
     * Const for markup type with keyboard
     */
    const MT_KEYBOARD = 1;

    /**
     * Const for markup type with inline keyboard
     */
    const MT_INLINE_KEYBOARD = 2;

    /**
     * @var array
     */
    private $_keyboard = [];

    public $markup_type = self::MT_KEYBOARD;

    public $one_time_keyboard = false;

    public $resize_keyboard = false;

    public $keyboard_columns = 1;

    /**
     * @return ReplyKeyboardMarkup|Markup
     */
    public function getMarkup()
    {
        switch ($this->markup_type) {
            case self::MT_INLINE_KEYBOARD:
                $markup = new Markup();
                $markup->inline_keyboard = $this->_keyboard;
                break;
            case self::MT_KEYBOARD:
            default:
                $markup = new ReplyKeyboardMarkup();
                $markup->keyboard = $this->_keyboard;
                $markup->one_time_keyboard = $this->one_time_keyboard;
                $markup->resize_keyboard = $this->resize_keyboard;
        }

        return $markup;
    }

    public function addKeyboardButtons(array $buttons)
    {
        if (count($buttons) !== 0) {
            foreach ($buttons as $rowId => $button) {
                $this->addKeyboardButton($button);
            }
        }
    }

    /**
     * @param array $data
     */
    public function addKeyboardButton(array $data)
    {
        if (isset($data['callback_data'])) $data['callback_data'] = Json::encode($data['callback_data']);

        $rows_count = count($this->_keyboard);
        if ($rows_count > 0)
            $current_row = (count($this->_keyboard[$rows_count-1]) < $this->keyboard_columns) ? $rows_count-1 : $rows_count;
        else
            $current_row = 0;
        $this->_keyboard[$current_row][] = $data;
    }
}