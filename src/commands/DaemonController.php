<?php
/**
 * Poject: ready-bot-v2
 * User: mitrii
 * Date: 1.19.2017
 * Time: 12:17
 * Original File Name: DaemonController.php
 */

namespace mitrii\bot\commands;

use mitrii\bot\helpers\Bot;
use mitrii\bot\jobs\UpdateJob;
use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;
use yii\console\Controller;
use yii\helpers\Console;

class DaemonController extends Controller
{
    /** @var LoopInterface */
    protected $loop;

    public function init()
    {
        parent::init();
        $this->loop = Factory::create();
    }

    public function actionIndex($show_memory = false)
    {
        Console::output('Starting bot: @' . \Yii::$app->botManager->nickname);

        if ($show_memory) $this->loop->addPeriodicTimer(60, function() {
            Console::output('Memory: ' . number_format(intval((memory_get_usage() / 1024)) / 1024, 2, '.', '') . 'Mb');
        });


        $this->loop->addPeriodicTimer(0.5, function() {
            \Yii::$app->queue->run(false);
        });


        $this->loop->addPeriodicTimer(1, function($timer) {
            try {
                $updates = \Yii::$app->botManager->getUpdates();
            } catch (Exception $e) {
                Console::error($e->getMessage());
            }

            if (is_array($updates)) {
                foreach ($updates as $update)
                {
                    \Yii::$app->queue->push(new UpdateJob([
                        'update' => $update,
                    ]));
                }
            }
        });

        $this->loop->run();
    }

}
