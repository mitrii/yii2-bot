<?php
namespace mitrii\modules\bot\events;

use yii\base\Event;

class MessageEvent extends Event
{
    /**
    * @var \TelegramBot\Api\Types\Message
    */
    
    public $message;
    
    public $command = false;
    
    public $args = [];

}