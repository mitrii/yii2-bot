<?php
namespace mitrii\modules\bot\events;

use yii\base\Event;

class UpdateEvent extends Event
{
    /**
     * @var \TelegramBot\Api\Types\Update
     */
    public $update;
}