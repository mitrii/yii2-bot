<?php
/**
 * Poject: ready-bot-v2
 * User: mitrii
 * Date: 1.26.2017
 * Time: 17:23
 * Original File Name: BotManagerInterface.php
 */

namespace mitrii\bot;


interface BotManagerInterface
{
    const EVENT_RECEIVE_UPDATE = 'update';
    const EVENT_MSG_TEXT = 'text';
    const EVENT_MSG_TEXT_COMMAND = 'command';
    const EVENT_MSG_AUDIO = 'audio';
    const EVENT_MSG_DOCUMENT = 'document';
    const EVENT_MSG_PHOTO = 'photo';
    const EVENT_MSG_STICKER = 'sticker';
    const EVENT_MSG_VIDEO = 'video';
    const EVENT_MSG_VOICE = 'voice';
    const EVENT_MSG_CAPTION = 'caption';
    const EVENT_MSG_CONTACT = 'contact';
    const EVENT_MSG_LOCATION = 'location';

    public function getApi();

    public function getNickname();

    public function getBot_id();

    public function getUpdateOffset();

    public function setUpdateOffset($value);

    public function getUpdates();
}