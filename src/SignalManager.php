<?php
/**
 * Poject: ready-bot-v2
 * User: mitrii
 * Date: 12.30.2016
 * Time: 17:08
 * Original File Name: CommandManager.php
 */

namespace mitrii\bot;

use Yii;
use yii\base\Component;

class SignalManager extends Component
{
    /**
     * @var array the rules for parsing bot commands
     */
    public $rules = [];

    /**
     * @var array the default configuration of command rules. Individual rule configurations
     * specified via [[rules]] will take precedence when the same property of the rule is configured.
     */
    public $ruleConfig = ['class' => 'mitrii\bot\SignalRule'];

    public function init()
    {
        parent::init();

        $this->rules = $this->buildRules($this->rules);
    }

    /**
     *
     */
    public function parseRequest($request)
    {
        foreach ($this->rules as $rule) {
            if (($result = $rule->parseRequest($this, $request)) !== false) {
                return $result;
            }
        }

        return [\Yii::$app->defaultRoute, []];
    }

    /**
     * Builds URL rule objects from the given rule declarations.
     * @param array $rules the rule declarations. Each array element represents a single rule declaration.
     * Please refer to [[rules]] for the acceptable rule formats.
     * @return CommandRuleInterface[] the rule objects built from the given rule declarations
     * @throws InvalidConfigException if a rule declaration is invalid
     */
    protected function buildRules($rules)
    {
        $compiledRules = [];
        $verbs = 'GET|HEAD|POST|PUT|PATCH|DELETE|OPTIONS';
        foreach ($rules as $key => $rule) {
            if (is_string($rule)) {
                $rule = ['route' => $rule];
                if (preg_match("/^((?:($verbs),)*($verbs))\\s+(.*)$/", $key, $matches)) {
                    $rule['type'] = explode(',', $matches[1]);
                    // rules that do not apply for GET requests should not be use to create urls
                    $key = $matches[4];
                }
                $rule['pattern'] = $key;
            }
            if (is_array($rule)) {
                $rule = Yii::createObject(array_merge($this->ruleConfig, $rule));
            }
            if (!$rule instanceof SignalRuleInterface) {
                throw new InvalidConfigException('Command rule class must implement SignalRuleInterface.');
            }
            $compiledRules[] = $rule;
        }
        return $compiledRules;
    }
}