<?php
/**
 * Poject: ready-bot-v2
 * User: mitrii
 * Date: 12.30.2016
 * Time: 17:46
 * Original File Name: CommandRule.php
 */

namespace mitrii\bot;

use Yii;
use yii\helpers\Json;
use yii\helpers\StringHelper;

class SignalRule extends \yii\base\Object implements SignalRuleInterface
{

    /**
     * @var string the route to the controller action
     */
    public $route;

    /**
     * @var string the name of this rule. If not set, it will use [[pattern]] as the name.
     */
    public $name;

    /**
     * On the rule initialization, the [[pattern]] matching parameters names will be replaced with [[placeholders]].
     * @var string the pattern used to parse and create the path info part of a URL.
     * @see host
     * @see placeholders
     */
    public $pattern;

    public $type;

    /**
     * @inheritdoc
     */
    public function parseRequest($manager, $request)
    {
        if ($request->type == $request::TYPE_CALLBACK_QUERY) {
            $data = Json::decode($request->update->callback_query->data);
            return [$data['route'], $data['params']];
        }


        $text = $request->getMessage()->text;

        $command = strtok($text, " \n\t");

        $params = [];
        do {
            $param = strtok(" \n\t");
            if ($param !== false) $params[] = $param;
        } while ($param !== false);

        if (!StringHelper::startsWith($command, $this->pattern, false)) {
            return false;
        }

        Yii::trace("Request parsed with command rule: {$this->name}", __METHOD__);

        return [$this->route, $params];
    }
}
